<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 15.05.17
 * Time: 2:46
 */

require 'vendor/autoload.php';

use RoosterParking\ChainOfResponsibility\Handler;

$tariffs = [
    [
        'id' => 1,
        'title' => 'One Hour',
        'night_offer_percent' => 30,
        'rest_day_offer_percent' => 10,
        'period_hours' => 1,
        'cost_per_hour' => 80
    ],

    [
        'id' => 2,
        'title' => 'One Day',
        'night_offer_percent' => 20,
        'rest_day_offer_percent' => 10,
        'period_hours' => 24,
        'cost_per_hour' => 10
    ],

    [
        'id' => 3,
        'title' => 'Six Hours',
        'night_offer_percent' => 30,
        'rest_day_offer_percent' => 10,
        'period_hours' => 6,
        'cost_per_hour' => 40
    ],

];

$dateIn = "2017-05-13T13:56:24+03:00";
$dateOut = date(DATE_ATOM, time());
$testTime = 26;

$handler = new Handler();
$handler->setNext(new Handler());
$handler->calculate($dateIn, $tariffs);


