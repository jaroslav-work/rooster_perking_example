<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 15.05.17
 * Time: 2:45
 */
namespace RoosterParking\helpers;

class DatetimeHelper
{
    public static function isNightHour($dateTime)
    {
        $hour = date('G', strtotime($dateTime));
        return !($hour >= 7 && $hour <= 19);
    }

    public static function isRestDayHour($dateTime)
    {
        $dayNum = date('N', strtotime($dateTime));
        return $dayNum > 5;
    }
}