<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 15.05.17
 * Time: 2:45
 */
namespace RoosterParking\ChainOfResponsibility;
use RoosterParking\helpers\DatetimeHelper;

class Handler extends AbstractHandler
{
    public $odd;
    public $dateFrom = null;

    protected function _calculate($hours, $tariffs)
    {
        $this->dateFrom = date(DATE_ATOM, strtotime("+{$hours} hour", time()));
        $SuitableTariff = $this->_getSuitableTariff($hours, $tariffs);
        $this->odd = $SuitableTariff['tariff']['period_hours'] * $SuitableTariff['quotient'];
        return $SuitableTariff;
    }

    protected function _getSuitableTariff($hours, $tariffs)
    {
        $tariff_rates = [];
        $results = [];
        foreach ($tariffs as $k => $tariff) {
            $quotient = intdiv($hours, $tariff['period_hours']);
            if ($quotient >= 1) {
                $results[] = [
                    'quotient' => $quotient,
                    'tariff' => $tariffs[$k]
                ];

                $tariff_rates[] = $quotient;
            }
        }

        $minTariffRate = min($tariff_rates);

        $SuitableTariff = null;
        foreach ($results as $result) {
            if ($result['quotient'] == $minTariffRate) {
                return $result;
            }
        }


    }

    protected function _getCost($SuitableTariff) {
        $cost = 0;
        $endDate = null;
        foreach (range(1, $SuitableTariff['tariff']['period_hours']) as $k => $item) {
            $cost_per_hour = $SuitableTariff['tariff']['cost_per_hour'];
            $final_cost_per_hour = 0;
            $date = date(DATE_ATOM, strtotime("+{$k} hour", strtotime($this->dateFrom))) . PHP_EOL;

            $night_percent = $cost_per_hour / 100 * $SuitableTariff['tariff']['night_offer_percent'];
            $rest_day_percent = $cost_per_hour / 100 * $SuitableTariff['tariff']['rest_day_offer_percent'];

            if(DatetimeHelper::isNightHour($date)) {
                $final_cost_per_hour += ($cost_per_hour + $night_percent);
            }

            if (DatetimeHelper::isRestDayHour($date)) {
                $final_cost_per_hour += ($cost_per_hour + $rest_day_percent);
            }

            $cost += $final_cost_per_hour;
            $endDate = $date;
        }

        return [
            'cost' => $cost,
            'endDate' => $endDate,
        ];
    }
}