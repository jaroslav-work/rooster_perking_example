<?php

/**
 * Created by PhpStorm.
 * User: inle
 * Date: 15.05.17
 * Time: 3:07
 */
namespace RoosterParking\ChainOfResponsibility;
abstract class AbstractHandler
{
    protected $_next;

    /**
     * @param AbstractHandler $logger
     * @return AbstractHandler
     */
    public function setNext(self $logger)
    {
        $this->_next = $logger;
        return $this->_next;
    }

    /**
     * @param $hours
     * @param $tariffs
     */
    public function calculate($hours, $tariffs)
    {
        $this->_calculate($hours, $tariffs);
        if ($this->_next !== null) {
            $this->_next->calculate($hours, $tariffs);
        }
    }
    
    abstract protected function _calculate($hours, $tariffs);
}